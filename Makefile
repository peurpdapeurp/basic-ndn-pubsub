make:
	g++ consumer.cpp -std=c++11 `pkg-config --cflags libndn-cxx` `pkg-config --libs libndn-cxx` -o consumer
	g++ producer.cpp -std=c++11 `pkg-config --cflags libndn-cxx` `pkg-config --libs libndn-cxx` -o producer

