
#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <ndn-cxx/encoding/block.hpp>
#include <ndn-cxx/util/notification-stream.hpp>

#include <iostream>
#include <string>

#include <sstream>

int counter = 0;
bool exiting = false;

void onInterest(const ndn::InterestFilter& filter, const ndn::Interest& interest) {

  std::cout << ">> I: " << interest << std::endl;

}

void onRegisterFailed(const ndn::Name& name, const std::string reason) {

  std::cout << "Register failed for " << name << 
    " on local daemon hub (" << reason << ")" << std::endl;

}


void *producerFunction(void *vargp) {

  std::cout << "producer function got called" << std::endl;

  void** pointerArray = (void**) vargp;

  ndn::util::NotificationStream<ndn::Data> *streamerPointer = 
    (ndn::util::NotificationStream<ndn::Data> *) pointerArray[0];

  ndn::Face *facePointer = (ndn::Face *) pointerArray[1];

  ndn::KeyChain *keyChainPointer = (ndn::KeyChain *) pointerArray[2];

  facePointer->setInterestFilter("/producer",
				 onInterest,
				 ndn::RegisterPrefixSuccessCallback(),
				 onRegisterFailed);


  while (true) {

    facePointer->processEvents();

    sleep(1);

  }
}

void * posterFunction(void* vargp) {
  
  std::cout << "poster function got called" << std::endl;

  void ** pointerArray = (void **) vargp;

  ndn::util::NotificationStream<ndn::Data> *streamerPointer = 
    (ndn::util::NotificationStream<ndn::Data> *) pointerArray[0];
  
  ndn::KeyChain *keyChainPointer = (ndn::KeyChain *) pointerArray[2];

  
  while (true) {
    
    ndn::Data content("insideData");
    keyChainPointer->sign(content);

    streamerPointer->postNotification(content);

    sleep(10);

  }

}

int main (int argc, char **argv) {

  std::cout << "Starting producer... " << std::endl;

  pthread_t tid1, tid2;

  void* pointerArray[3];

  ndn::Face testFace;
  ndn::Name name("producer");
  ndn::KeyChain keyChain;

  ndn::util::NotificationStream<ndn::Data> streamer(testFace, name, keyChain);

  pointerArray[0] = (void *) &streamer;
  pointerArray[1] = (void *) &testFace;
  pointerArray[2] = (void *) &keyChain;

  pthread_create(&tid1, NULL, producerFunction, pointerArray);
  pthread_create(&tid2, NULL, posterFunction, pointerArray); 

  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);

  return 0;

}
