
#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <ndn-cxx/util/notification-subscriber.hpp>

#include <iostream>
#include <string>

void notifCallBack(ndn::Data data) {

  std::cout << "got data:" << data << std::endl;
  
}

void onNackCallback(ndn::lp::Nack nack) {

  std::cout << "got nack" << std::endl;

}

void onTimeoutCallback() {

  //std::cout << "got timeout" << std::endl;

}

void onDecodeErrorCallback(ndn::Data data) {

  std::cout << "decoding error" << std::endl;

  std::cout << "data: " << data << std::endl;

}

void *consumerFunction(void *vargp) {

  void ** pointerArray = (void**) vargp;

  ndn::Face* facePointer = (ndn::Face*) pointerArray[0];
  ndn::util::NotificationSubscriber<ndn::Data>* subberPointer = 
    (ndn::util::NotificationSubscriber<ndn::Data>*) pointerArray[1];

  subberPointer->onNotification.connect(notifCallBack);
  subberPointer->onNack.connect(onNackCallback);
  subberPointer->onTimeout.connect(onTimeoutCallback);
  subberPointer->onDecodeError.connect(onDecodeErrorCallback);

  subberPointer->start();

  while (true) {

    facePointer->processEvents();

    sleep(1);

  }

}

int main (int argc, char **argv) {

  std::cout << "Starting consumer... " << std::endl;

  ndn::Face testFace;
  ndn::Name name("/producer");
  ndn::time::milliseconds lifetime(1000);
  ndn::util::NotificationSubscriber<ndn::Data> subber(testFace, name, lifetime);

  pthread_t tid1;

  void* pointerArray[2];

  pointerArray[0] = &testFace;
  pointerArray[1] = &subber;

  pthread_create(&tid1, NULL, consumerFunction, (void *) pointerArray);

  pthread_join(tid1, NULL);

  return 0;

}
